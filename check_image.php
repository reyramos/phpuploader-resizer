<?php
/**
 * Created by PhpStorm.
 * User: rramos
 * Date: 11/21/13
 * Time: 3:13 PM
 */

$dir = dirname(__FILE__) . '/images';
$thumbdir = $dir . '/thumbs';


if ($_FILES['uploadfile']['error'] != UPLOAD_ERR_OK) {
	switch ($_FILES['uploadfile']['error']) {

		case UPLOAD_ERR_INI_SIZE:
			die("The uploaded file exceeds the upload_max_filesize directive in php.ini.");
			break;
		case UPLOAD_ERR_FORM_SIZE:
			die("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.");
			break;
		case UPLOAD_ERR_PARTIAL:
			die("The uploaded file was only partially uploaded.");
			break;
		case UPLOAD_ERR_NO_FILE:
			die("No file was uploaded.");
			break;
		case UPLOAD_ERR_NO_TMP_DIR:
			break;
		case UPLOAD_ERR_CANT_WRITE:
			die("Missing a temporary folder");
			break;
		case UPLOAD_ERR_EXTENSION:
			die("A PHP extension stopped the file upload.");
			break;
	}
}


list($width, $height, $type, $attr) = getimagesize($_FILES['uploadfile']['tmp_name']);

switch ($type) {

	case IMAGETYPE_GIF:
		$image = imagecreatefromgif($_FILES['uploadfile']['tmp_name']);
		imagegif($image,$dir."/".$_FILES['uploadfile']['name']);

		break;
	case IMAGETYPE_JPEG:
		$image = imagecreatefromjpeg($_FILES['uploadfile']['tmp_name']);
		imagejpeg($image,$dir."/".$_FILES['uploadfile']['name'], 100);

		break;
	case IMAGETYPE_PNG:
		$image = imagecreatefrompng($_FILES['uploadfile']['tmp_name']);
		imagepng($image,$dir."/".$_FILES['uploadfile']['name']);

		break;
	default:

		//move_uploaded_file($_FILES['uploadfile']['tmp_name'],$thumbdir."/".$_FILES['uploadfile']['name']);
		die('unsupported image file');
		break;

}


imagejpeg($original,$dir."/".$_FILES['uploadfile']['name'], 100);

//set the dimensions for the thumbnail
$thumb_width = $width * 0.10;
$thumb_height = $height * 0.10;

//create the thumbnail
$thumb = imagecreatetruecolor($thumb_width, $thumb_height);
imagecopyresampled($thumb, $image, 0, 0, 0, 0, $thumb_width,$thumb_height,$width, $height);

imagejpeg($thumb,$thumbdir."/".$_FILES['uploadfile']['name']);

imagedestroy($image);
imagedestroy($thumb);


header("LOCATION:index.html");